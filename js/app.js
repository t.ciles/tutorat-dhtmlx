/**
 * 
 * @param {dhtmlXDataStore} store 
 */
function showAccueil(store) {
    const layout = new dhtmlXLayoutObject(document.body, '1C');

    const tabbar = layout.cells('a').attachTabbar();
    tabbar.addTab("accueil", "Accueil", null, null, true, false);

    const contentLayout = tabbar.tabs('accueil').attachLayout('2E');
    contentLayout.cells('a').fixSize(true, true);
    contentLayout.cells('a').setHeight(50);
    contentLayout.cells('a').hideHeader();
    contentLayout.cells('b').hideHeader();

    const grid = contentLayout.cells('b').attachGrid();
    grid.setHeader('Etablissement,Adresse,Adresse (Suite),Code Postal,Ville');
    grid.setColumnIds('etablissement,adresse,adresse_suite,code_postal,ville');
    grid.setColTypes('ro,ro,ro,ro,ro');
    grid.init();
    grid.sync(store);
    grid.attachEvent('onRowDblClicked', (rId, cIdx) => {
        const agence = store.item(rId);
        showAgence(tabbar, agence);
    });

    const form = contentLayout.cells('a').attachForm([
        {type:"input", name:"search_text", value:""},
        {type:"button", name:"search_button", value:"Rechercher", position: "absolute", inputLeft: 200, inputTop: 10},
    ]);
    form.attachEvent("onButtonClick", (id) => {
        grid.filterBy(0, form.getItemValue('search_text'));
    });
}

/**
 * 
 * @param {dhtmlXTabBar} tabbar 
 * @param {object} agence 
 */
function showAgence(tabbar, agence) {
    tabbar.addTab("agence-" + agence.id, agence.etablissement, null, null, true, true);

    const agenceLayout = tabbar.tabs("agence-" + agence.id).attachLayout('2U');
    agenceLayout.cells('b').hideHeader();
    agenceLayout.cells('a').hideHeader();
    agenceLayout.cells('a').setWidth(250);
    agenceLayout.cells('a').fixSize(true, true);

    const treeItems = agence.collaborateurs
        .reduce((acc, el) => acc.concat({id: el.id, text: el.nom + ' ' + el.prenom}), []);

    const tree = agenceLayout.cells('a').attachTreeView({
        items: [{id: 0, text: 'Etablissement', open: 1, items: treeItems}]
    });

    tree.attachEvent('onClick', (id) => {
        if (id === 0) {
            return false;
        }

        const collaborateur = agence.collaborateurs.find((el) => el.id === id);
        
        if (!Object.keys(collaborateur).length) {
            return false;
        }

        // Reset content on collaborateur click.
        agenceLayout.cells('b').detachObject(true);
        showCollaborateur(agenceLayout, collaborateur);
    });
}

/**
 * 
 * @param {dhtmlXLayoutObject} agenceLayout 
 * @param {object} collaborateur 
 */
function showCollaborateur(agenceLayout, collaborateur) {
    const editButton = document.createElement('button');
    editButton.innerText = 'Editer';
    editButton.style.margin = '20px';

    editButton.addEventListener('click', function(e) {
        e.preventDefault();

        const evalPopupWindow = agenceLayout.dhxWins.createWindow("evalPopupWindow", 0, 0, 600, 400);
        evalPopupWindow.centerOnScreen();
        evalPopupWindow.setText('Collaborateur');
        evalPopupWindow.setModal(true);

        const evalLayout = evalPopupWindow.attachLayout('2E');
        evalLayout.cells('b').hideHeader(true);
        evalLayout.cells('a').hideHeader(true);
        evalLayout.cells('b').setHeight(50);
        evalLayout.cells('b').fixSize(true, true);

        const evalGrid = evalLayout.cells('a').attachGrid();
        evalGrid.setHeader('Details,Avis');
        evalGrid.setColumnIds('details,avis');
        evalGrid.setColTypes('ro,ro');
        evalGrid.init();
        evalGrid.sync(new dhtmlXDataStore({data: collaborateur.evaluation}));

        addButtonToCellLayout(evalLayout.cells('b'), 'Annuler', (pEvent) => {
            evalPopupWindow.close();
        });
        addButtonToCellLayout(evalLayout.cells('b'), 'Editer', (pEvent) => {
            showCollaborateurPrint(evalPopupWindow, collaborateur);
        });
    });

    const html = document.querySelector('.collaborateur_template').cloneNode(true);
    html.querySelector('.nom').innerText = collaborateur.nom;
    html.querySelector('.prenom').innerText = collaborateur.prenom;
    html.querySelector('.role').innerText = collaborateur.role;
    html.querySelector('.date_entree').innerText = collaborateur.date_entree;
    html.querySelector('.photo').src = collaborateur.photo;

    agenceLayout.cells('b').appendObject(editButton);    
    agenceLayout.cells('b').appendObject(html);
}

/**
 * 
 * @param {dhtmlXWindows} agenceLayout 
 * @param {object} collaborateur 
 */
function showCollaborateurPrint(evalPopupWindow, collaborateur) {
    const printLayout = evalPopupWindow.attachLayout('1C');
    printLayout.cells('a').hideHeader(true);
    printLayout.cells('a').attachURL('my.pdf');

    evalPopupWindow.setDimension(960, 768);
    evalPopupWindow.centerOnScreen();
}

/**
 * 
 * @param {dhtmlXLayoutCell} cellLayout 
 * @param {String} text 
 * @param {function} onClik 
 */
function addButtonToCellLayout(cellLayout, text, onClik) {
    const button = document.createElement('button');
    button.innerText = text;
    button.addEventListener('click', onClik);

    cellLayout.appendObject(button);
}
